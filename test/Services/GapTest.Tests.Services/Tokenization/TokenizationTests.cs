using System;
using System.Collections.Generic;
using GapTest.Services.Tokenization;
using GapTest.Services.Tokenization.Implementations;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GapTest.Tests.Services.Tokenization
{
    public class TokenizationTests
    {
        private readonly ITokenizationService _tokenizationService; 
        
        public TokenizationTests()
        {
            var configurationValues = new Dictionary<string, string>
            {
                {"AppSettings:JWT_SECRET", "This is a long text that supplies the JWT secret"}
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationValues)
                .Build();
            
            _tokenizationService = new JwtTokenizationService(configuration);
        }

        [Fact]
        public void ShouldGenerateToken()
        {
            string token = _tokenizationService.GenerateToken(Guid.NewGuid(), "Admin");
            
            Assert.NotNull(token);
        }
    }
}