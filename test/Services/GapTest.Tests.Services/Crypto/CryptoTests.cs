using GapTest.Services.Crypto;
using GapTest.Services.Crypto.Implementations;
using Xunit;

namespace GapTest.Tests.Services.Crypto
{
    public class CryptoTests
    {
        private readonly IRfc2898HashService _rfc2898HashService;

        public CryptoTests()
        {
            _rfc2898HashService = new Rfc2898HashService();
        }
        
        [Fact]
        public void ShouldGenerateSaltWithDefault()
        {
            var salt = _rfc2898HashService.GenerateSalt();
            
            Assert.NotNull(salt);
            Assert.False(string.IsNullOrEmpty(salt));
        }
        
        [Fact]
        public void ShouldGenerateSaltWithIterations()
        {
            var salt = _rfc2898HashService.GenerateSalt(1000);
            
            Assert.NotNull(salt);
            Assert.False(string.IsNullOrEmpty(salt));
        }

        [Fact]
        public void ShouldGenerateHashWithSalt()
        {
            string salt = "3E8.Iyvw3HCEm5e5f5fGQE1ShA==";

            var hashedValue = _rfc2898HashService.GenerateHash("Hello world", salt);
            
            Assert.NotNull(hashedValue);
        }
        
        [Fact]
        public void ShouldGenerateHashWithOutputSalt()
        {
            var hashedValue = _rfc2898HashService.GenerateHash("Hello world", out var salt);
            
            Assert.NotNull(hashedValue);
            Assert.NotNull(salt);
        }
    }
}