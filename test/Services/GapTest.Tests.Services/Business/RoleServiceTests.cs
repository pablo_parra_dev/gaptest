using System.Collections.Generic;
using GapTest.Services.Business;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Business.Implementations;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using Moq;
using Xunit;

namespace GapTest.Tests.Services.Business
{
    public class RoleServiceTests
    {
        private readonly IRoleService _roleService;

        public RoleServiceTests()
        {
            var roles = new List<Role>()
            {
                new Role() {Id = 1, Description = "Rol 1", Active = true},
                new Role() {Id = 2, Description = "Rol 2", Active = true}
            };

            var mockRolesRepository = new Mock<IRolesRepository>();

            mockRolesRepository.Setup(a => a.GetActive(It.IsAny<bool>())).Returns(roles);

            mockRolesRepository.Setup(a => a.GetById(It.Is<int>(x => x == 1))).Returns(
                new Role()
                {
                    Id = 1,
                    Description = "Role 1",
                    Active = true
                }
            );

            mockRolesRepository.Setup(a => a.GetById(It.Is<int>(y => y == 100))).Returns(
                (Role) null
            );

            var mockDtoConverter = new Mock<IRoleDtoConverter>();

            mockDtoConverter.Setup(a => a.ToDto(It.IsAny<IEnumerable<Role>>())).Returns(
                new List<RoleDto>()
                {
                    new RoleDto() {Id = 1, Description = "Rol 1", Active = true},
                    new RoleDto() {Id = 2, Description = "Rol 2", Active = true}
                });

            mockDtoConverter.Setup(a => a.ToDto(It.IsNotNull<Role>())).Returns(
                new RoleDto()
                {
                    Id = 1,
                    Description = "Role 1",
                    Active = true
                });

            mockDtoConverter.Setup(a => a.ToDto(It.Is<Role>(x => x == null))).Returns(
                (RoleDto) null
            );

            _roleService = new RoleService(mockRolesRepository.Object, mockDtoConverter.Object);
        }

        [Fact]
        public void ShouldReturnActiveRoles()
        {
            var result = _roleService.GetActiveRoles();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldReturnSingleRole()
        {
            var result = _roleService.GetRoleById(1);

            Assert.NotNull(result);
        }
        
        [Fact]
        public void ShouldReturnNullRole()
        {
            var result = _roleService.GetRoleById(2);

            Assert.Null(result);
        }
    }
}