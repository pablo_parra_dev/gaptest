using System;
using System.Collections.Generic;
using GapTest.Services.Business;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Business.Implementations;
using GapTest.Services.Crypto;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using Moq;
using Xunit;

namespace GapTest.Tests.Services.Business
{
    public class UserServiceTests
    {
        private readonly IUserService _userService;

        public UserServiceTests()
        {
            var mockUserRepository = new Mock<IUsersRepository>();
            var mockUserDtoConverter = new Mock<IUserDtoConverter>();
            var mockUserWithPasswordDtoConverter = new Mock<IUserWithPasswordDtoConverter>();
            var mockHashService = new Mock<IRfc2898HashService>();

            mockUserRepository.Setup(a => a.GetByUserName(It.Is<string>(x => x == "not_found_user")))
                .Returns((User) null);

            mockUserRepository.Setup(a => a.GetByUserName(It.Is<string>(x => x == "found_user")))
                .Returns(new User()
                {
                    Id = Guid.NewGuid(),
                    Password = "e1vpndA2n08teJeiLkpji348BQzDbdkL",
                    Salt = "3E8.o8jguJmqeROc2+HJG7QiRA==",
                    CreationDate = DateTime.Now,
                    FirstName = "Found",
                    LastName = "User",
                    RoleId = 1,
                    UserName = "found_user"
                });

            mockUserRepository.Setup(a =>
                    a.GetById(It.Is<Guid>(x => x == new Guid("b3313b65-5de3-4d7b-acdb-d0f0e9f2f62d"))))
                .Returns(new User()
                {
                    Id = Guid.NewGuid(),
                    Password = "e1vpndA2n08teJeiLkpji348BQzDbdkL",
                    Salt = "3E8.o8jguJmqeROc2+HJG7QiRA==",
                    CreationDate = DateTime.Now,
                    FirstName = "Found",
                    LastName = "User",
                    RoleId = 1,
                    UserName = "found_user"
                });

            mockUserRepository.Setup(a =>
                    a.GetById(It.Is<Guid>(x => x == new Guid("17de63ce-4ff0-4586-8c04-fbeb98c4a177"))))
                .Returns(
                    (User) null
                );

            mockUserRepository.Setup(a => a.GetAll()).Returns(
                new List<User>()
                {
                    new User()
                    {
                        Id = Guid.NewGuid(),
                        CreationDate = DateTime.Now,
                        FirstName = "Test",
                        LastName = "User",
                        RoleId = 1,
                        UserName = "test_user",
                        Password = "123",
                        Salt = "1234"
                    },
                    new User()
                    {
                        Id = Guid.NewGuid(),
                        CreationDate = DateTime.Now,
                        FirstName = "Test",
                        LastName = "User 2",
                        RoleId = 2,
                        UserName = "test_user2",
                        Password = "123",
                        Salt = "1234"
                    }
                }
            );

            mockUserDtoConverter.Setup(a => a.ToDto(It.IsNotNull<User>())).Returns(
                new UserDto()
                {
                    Id = Guid.NewGuid(),
                    CreationDate = DateTime.Now,
                    FirstName = "Test",
                    LastName = "User",
                    RoleId = 2,
                    UserName = "test_user"
                }
            );

            mockUserDtoConverter.Setup(a => a.ToDto(It.Is<User>(x => x == null))).Returns(
                (UserDto) null
            );

            mockUserDtoConverter.Setup(a => a.ToDto(It.IsAny<IEnumerable<User>>())).Returns(
                new List<UserDto>()
                {
                    new UserDto()
                    {
                        Id = Guid.NewGuid(),
                        CreationDate = DateTime.Now,
                        FirstName = "Test",
                        LastName = "User",
                        RoleId = 1,
                        UserName = "test_user"
                    },
                    new UserDto()
                    {
                        Id = Guid.NewGuid(),
                        CreationDate = DateTime.Now,
                        FirstName = "Test",
                        LastName = "User 2",
                        RoleId = 2,
                        UserName = "test_user2"
                    }
                }
            );

            mockUserWithPasswordDtoConverter.Setup(a => a.ToDto(It.IsNotNull<User>())).Returns(
                new UserWithPasswordDto()
                {
                    Id = Guid.NewGuid(),
                    CreationDate = DateTime.Now,
                    FirstName = "Test",
                    LastName = "User",
                    RoleId = 2,
                    UserName = "test_user",
                    Password = "e1vpndA2n08teJeiLkpji348BQzDbdkL",
                    Salt = "3E8.o8jguJmqeROc2+HJG7QiRA=="
                }
            );

            mockUserWithPasswordDtoConverter.Setup(a => a.ToDto(It.Is<User>(x => x == null))).Returns(
                (UserWithPasswordDto) null
            );

            mockHashService.Setup(a => a.GenerateHash(It.Is<string>(x => x == "123"),
                It.Is<string>(y => y == "3E8.o8jguJmqeROc2+HJG7QiRA=="))).Returns(
                "e1vpndA2n08teJeiLkpji348BQzDbdkL"
            );

            mockHashService.Setup(a => a.GenerateHash(It.Is<string>(x => x == "1234"),
                It.Is<string>(y => y == "3E8.o8jguJmqeROc2+HJG7QiRA=="))).Returns(
                "asdasdasdasd"
            );

            _userService = new UserService(mockUserRepository.Object, mockUserDtoConverter.Object,
                mockUserWithPasswordDtoConverter.Object, mockHashService.Object);
        }

        [Fact]
        public void ShouldValidateCredentialsSuccess()
        {
            bool result = _userService.ValidateCredentials("found_user", "123");

            Assert.True(result);
        }

        [Fact]
        public void ShouldValidateCredentialsNotFoundUser()
        {
            bool result = _userService.ValidateCredentials("not_found_user", "123");

            Assert.False(result);
        }

        [Fact]
        public void ShouldValidateCredentialsPasswordIncorrect()
        {
            bool result = _userService.ValidateCredentials("not_found_user", "1234");

            Assert.False(result);
        }

        [Fact]
        public void ShouldReturnByUserNameSuccess()
        {
            var result = _userService.GetUserByUserName("found_user");

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldReturnNullByUserName()
        {
            var result = _userService.GetUserByUserName("not_found_user");

            Assert.Null(result);
        }

        [Fact]
        public void ShouldReturnByIdSuccess()
        {
            var result = _userService.GetUserById(new Guid("b3313b65-5de3-4d7b-acdb-d0f0e9f2f62d"));

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldReturnNullById()
        {
            var result = _userService.GetUserById(new Guid("17de63ce-4ff0-4586-8c04-fbeb98c4a177"));

            Assert.Null(result);
        }

        [Fact]
        public void ShouldReturnWithPasswordByIdSuccess()
        {
            var result = _userService.GetUserWithPasswordById(new Guid("b3313b65-5de3-4d7b-acdb-d0f0e9f2f62d"));

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldReturnWithPasswordNullById()
        {
            var result = _userService.GetUserWithPasswordById(new Guid("17de63ce-4ff0-4586-8c04-fbeb98c4a177"));

            Assert.Null(result);
        }
    }
}