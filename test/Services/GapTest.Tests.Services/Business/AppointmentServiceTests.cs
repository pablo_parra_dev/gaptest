using System;
using System.Collections.Generic;
using System.Linq;
using GapTest.Services.Business;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Business.Implementations;
using GapTest.Services.Commons;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using Moq;
using Xunit;

namespace GapTest.Tests.Services.Business
{
    public class AppointmentServiceTests
    {
        private readonly IAppointmentService _appointmentService;

        public AppointmentServiceTests()
        {
            var mockAppointmentRepository = new Mock<IAppointmentsRepository>();
            var mockAppointmentDtoConverter = new Mock<IAppointmentDtoConverter>();

            mockAppointmentRepository.Setup(a =>
                    a.GetByUserId(It.Is<Guid>(x => x == new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a"))))
                .Returns(
                    new List<Appointment>()
                    {
                        new Appointment()
                        {
                            Id = Guid.NewGuid(),
                            AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                            AppointmentTypeId = 1,
                            CreationDate = DateTime.Now.AddDays(-10),
                            UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                        },
                        new Appointment()
                        {
                            Id = Guid.NewGuid(),
                            AppointmentDate = new DateTime(2020, 2, 15, 14, 0, 0),
                            AppointmentTypeId = 1,
                            CreationDate = DateTime.Now.AddDays(-20),
                            UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                        }
                    });

            mockAppointmentRepository.Setup(a =>
                a.GetByUserId(It.Is<Guid>(x => x == Guid.Empty))).Returns(
                (List<Appointment>) null
            );

            mockAppointmentRepository.Setup(a =>
                    a.GetById(It.Is<Guid>(x => x == new Guid("eaffa9fa-fb1e-4bbb-954a-8d7c6d8d41dd"))))
                .Returns(
                    new Appointment()
                    {
                        Id = new Guid("eaffa9fa-fb1e-4bbb-954a-8d7c6d8d41dd"),
                        AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                        AppointmentTypeId = 1,
                        CreationDate = DateTime.Now.AddDays(-10),
                        UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                    }
                );
            
            mockAppointmentRepository.Setup(a =>
                    a.GetById(It.Is<Guid>(x => x == new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875"))))
                .Returns(
                    new Appointment()
                    {
                        Id = new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875"),
                        AppointmentDate = new DateTime(2020, 2, 20, 14, 0, 0),
                        AppointmentTypeId = 1,
                        CreationDate = DateTime.Now.AddDays(-10),
                        UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                    }
                );
            
            mockAppointmentRepository.Setup((a =>
                    a.GetById(It.Is<Guid>(x => x == Guid.Empty))))
                .Returns(
                    (Appointment) null
                );

            mockAppointmentRepository.Setup(a =>
                    a.Delete(It.Is<Guid>(x => x == new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875"))))
                .Returns(
                    new Appointment()
                    {
                        Id = new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875"),
                        AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                        AppointmentTypeId = 1,
                        CreationDate = DateTime.Now.AddDays(-10),
                        UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                    }
                );

            mockAppointmentRepository.Setup(a =>
                    a.Delete(It.Is<Guid>(x => x == Guid.Empty)))
                .Returns((Appointment) null);

            mockAppointmentRepository.Setup(a =>
                    a.Add(It.IsAny<Appointment>()))
                .Returns<Appointment>(r => r);

            mockAppointmentDtoConverter.Setup(a =>
                    a.ToDto(It.IsNotNull<Appointment>()))
                .Returns(
                    new AppointmentDto()
                    {
                        Id = new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875"),
                        AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                        AppointmentTypeId = 1,
                        CreationDate = DateTime.Now.AddDays(-10),
                        UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                    }
                );
            
            mockAppointmentDtoConverter.Setup(a =>
                    a.ToDto(It.IsNotNull<IEnumerable<Appointment>>()))
                .Returns(
                    new List<AppointmentDto>()
                    {
                        new AppointmentDto()
                        {
                            Id = Guid.NewGuid(),
                            AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                            AppointmentTypeId = 1,
                            CreationDate = DateTime.Now.AddDays(-10),
                            UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                        },
                        new AppointmentDto()
                        {
                            Id = Guid.NewGuid(),
                            AppointmentDate = new DateTime(2020, 2, 15, 14, 0, 0),
                            AppointmentTypeId = 1,
                            CreationDate = DateTime.Now.AddDays(-20),
                            UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                        }
                    }
                );

            mockAppointmentDtoConverter.Setup(a =>
                    a.ToDto(It.Is<Appointment>(x => x == null)))
                .Returns(
                    (AppointmentDto) null
                );

            mockAppointmentDtoConverter.Setup(a => a.ToEntity(It.IsNotNull<AppointmentDto>()))
                .Returns(
                    new Appointment()
                    {
                        Id = new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875"),
                        AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                        AppointmentTypeId = 1,
                        CreationDate = DateTime.Now.AddDays(-10),
                        UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a")
                    }
                );

            mockAppointmentDtoConverter.Setup(a => a.ToEntity(It.Is<AppointmentDto>(x => x == null)))
                .Returns(
                    (Appointment) null
                );

            _appointmentService =
                new AppointmentService(mockAppointmentRepository.Object, mockAppointmentDtoConverter.Object);
        }

        [Fact]
        public void ShouldCreateAppointmentSuccess()
        {
            AppointmentDto input = new AppointmentDto()
            {
                Id = new Guid(),
                AppointmentDate = new DateTime(2020, 2, 20, 10, 0, 0),
                CreationDate = DateTime.Now,
                UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a"),
                AppointmentTypeId = 1
            };

            var result = _appointmentService.CreateNewAppointment(input);
            
            Assert.NotNull(result);
            Assert.Equal(ValidatableResponseStatus.Ok, result.Status);
            Assert.Null(result.Errors);
            Assert.NotNull(result.Response);
        }
        
        [Fact]
        public void ShouldFailCreationSameDayDate()
        {
            AppointmentDto input = new AppointmentDto()
            {
                Id = Guid.NewGuid(),
                AppointmentDate = new DateTime(2020, 2, 7, 14, 0, 0),
                CreationDate = DateTime.Now,
                UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a"),
                AppointmentTypeId = 1
            };

            var result = _appointmentService.CreateNewAppointment(input);
            
            Assert.NotNull(result);
            Assert.Equal(ValidatableResponseStatus.Failed, result.Status);
            Assert.NotNull(result.Errors);
            Assert.Null(result.Response);
            Assert.True(result.Errors.FirstOrDefault() == "DUPLICATED_APPOINTMENT");
        }
        
        [Fact]
        public void ShouldFailCreationPastDate()
        {
            AppointmentDto input = new AppointmentDto()
            {
                Id = Guid.NewGuid(),
                AppointmentDate = new DateTime(2020, 2, 1, 14, 0, 0),
                CreationDate = DateTime.Now,
                UserId = new Guid("84e61bb0-71ab-4158-b950-2ba589d3372a"),
                AppointmentTypeId = 1
            };

            var result = _appointmentService.CreateNewAppointment(input);
            
            Assert.NotNull(result);
            Assert.Equal(ValidatableResponseStatus.Failed, result.Status);
            Assert.NotNull(result.Errors);
            Assert.Null(result.Response);
            Assert.True(result.Errors.FirstOrDefault() == "PAST_APPOINTMENT_DATE");
        }

        [Fact]
        public void ShouldCancelSuccess()
        {
            var input = new Guid("8bbc49ad-3e46-4655-b870-37c640b5d875");

            var result = _appointmentService.CancelAppointment(input);
            
            Assert.NotNull(result);
            Assert.Equal(ValidatableResponseStatus.Ok, result.Status);
            Assert.Null(result.Errors);
            Assert.NotNull(result.Response);
        }
        
        [Fact]
        public void ShouldFailCancelByTimeout()
        {
            var input = new Guid("eaffa9fa-fb1e-4bbb-954a-8d7c6d8d41dd");

            var result = _appointmentService.CancelAppointment(input);
            
            Assert.NotNull(result);
            Assert.Equal(ValidatableResponseStatus.Failed, result.Status);
            Assert.NotNull(result.Errors);
            Assert.Null(result.Response);
            Assert.True(result.Errors.FirstOrDefault() == "APPOINTMENT_CANCELLATION_TIMEOUT");
        }
        
        [Fact]
        public void ShouldFailCancelByNotFound()
        {
            var input = Guid.Empty;

            var result = _appointmentService.CancelAppointment(input);
            
            Assert.NotNull(result);
            Assert.Equal(ValidatableResponseStatus.Failed, result.Status);
            Assert.NotNull(result.Errors);
            Assert.Null(result.Response);
            Assert.True(result.Errors.FirstOrDefault() == "APPOINTMENT_NOT_FOUND");
        }
    }
}