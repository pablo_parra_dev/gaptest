using System.Collections.Generic;
using GapTest.Services.Business;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Business.Implementations;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using Moq;
using Xunit;

namespace GapTest.Tests.Services.Business
{
    public class AppointmentTypeServiceTests
    {
        private readonly IAppointmentTypeService _appointmentTypeService;

        public AppointmentTypeServiceTests()
        {
            var appointmentTypes = new List<AppointmentType>()
            {
                new AppointmentType() {Id = 1, Description = "Tipo cita 1", Active = true},
                new AppointmentType() {Id = 2, Description = "Tipo cita 2", Active = true}
            };

            var mockAppointmentTypesRepository = new Mock<IAppointmentTypesRepository>();

            mockAppointmentTypesRepository.Setup(a => a.GetActive(It.IsAny<bool>())).Returns(appointmentTypes);

            mockAppointmentTypesRepository.Setup(a => a.GetById(It.Is<int>(x => x == 1))).Returns(
                new AppointmentType()
                {
                    Id = 1,
                    Description = "Tipo cita 1",
                    Active = true
                }
            );

            mockAppointmentTypesRepository.Setup(a => a.GetById(It.Is<int>(y => y == 100))).Returns(
                (AppointmentType) null
            );

            var mockDtoConverter = new Mock<IAppointmentTypeDtoConverter>();

            mockDtoConverter.Setup(a => a.ToDto(It.IsAny<IEnumerable<AppointmentType>>())).Returns(
                new List<AppointmentTypeDto>()
                {
                    new AppointmentTypeDto() {Id = 1, Description = "Tipo cita 1", Active = true},
                    new AppointmentTypeDto() {Id = 2, Description = "Tipo cita 2", Active = true}
                });

            mockDtoConverter.Setup(a => a.ToDto(It.IsNotNull<AppointmentType>())).Returns(
                new AppointmentTypeDto()
                {
                    Id = 1,
                    Description = "Tipo cita 1",
                    Active = true
                });

            mockDtoConverter.Setup(a => a.ToDto(It.Is<AppointmentType>(x => x == null))).Returns(
                (AppointmentTypeDto) null
            );

            _appointmentTypeService = new AppointmentTypeService(mockAppointmentTypesRepository.Object, mockDtoConverter.Object);
        }

        [Fact]
        public void ShouldReturnActiveAppointmentTypes()
        {
            var result = _appointmentTypeService.GetActiveAppointmentTypes();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldReturnSingleAppointmentType()
        {
            var result = _appointmentTypeService.GetAppointmentTypeById(1);

            Assert.NotNull(result);
        }
        
        [Fact]
        public void ShouldReturnNullAppointmentType()
        {
            var result = _appointmentTypeService.GetAppointmentTypeById(2);

            Assert.Null(result);
        }
    }
}