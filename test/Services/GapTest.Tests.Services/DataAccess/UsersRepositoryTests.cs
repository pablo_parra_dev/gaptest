using System;
using System.Collections.Generic;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using GapTest.Services.DataAccess.Implementations;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GapTest.Tests.Services.DataAccess
{
    public class UsersRepositoryTests
    {
        private readonly IUsersRepository _usersRepository;

        public UsersRepositoryTests()
        {
            var configurationValues = new Dictionary<string, string>
            {
                {
                    "AppSettings:ConnectionString",
                    "Server=localhost;Database=ClinicDates;User Id=sa;Password=Michoacan123*;"
                }
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationValues)
                .Build();

            _usersRepository = new UsersRepository(configuration);
        }

        [Fact]
        public void ShouldGetAll()
        {
            var result = _usersRepository.GetAll();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldGetById()
        {
            var result = _usersRepository.GetById(new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"));

            Assert.NotNull(result);
            Assert.Equal("bobbytables123", result.UserName);
        }

        [Fact]
        public void ShouldGetNullById()
        {
            var result = _usersRepository.GetById(Guid.Empty);

            Assert.Null(result);
        }

        [Fact]
        public void ShouldGetByUserName()
        {
            var result = _usersRepository.GetByUserName("pepitoperez123");

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldGetNullByUserName()
        {
            var result = _usersRepository.GetByUserName("non_existent_username");

            Assert.Null(result);
        }

        [Fact]
        public void ShouldInsert()
        {
            if (_usersRepository.GetByUserName("pepitoperez123") != null)
            {
                _usersRepository.Delete("387d191b-719e-4be4-81bd-c4e19684a92a");
            }
            
            var input = new User()
            {
                Id = new Guid("387d191b-719e-4be4-81bd-c4e19684a92a"),
                CreationDate = DateTime.Now,
                Password = "1234",
                Salt = "1234",
                FirstName = "Pepito",
                LastName = "Perez",
                RoleId = 1,
                UserName = "pepitoperez123"
            };

            var result = _usersRepository.Add(input);

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldUpdate()
        {
            var input = _usersRepository.GetById(new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"));

            if (input == null)
            {
                input = _usersRepository.Add(new User()
                {
                    Id = new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"),
                    CreationDate = DateTime.Now,
                    Password = "e1vpndA2n08teJeiLkpji348BQzDbdkL",
                    Salt = "3E8.o8jguJmqeROc2+HJG7QiRA==",
                    FirstName = "Bobby",
                    LastName = "Tables",
                    RoleId = 1,
                    UserName = "bobbytables123"
                });
            }

            input.Password = "123456789";
            input.RoleId = 2;

            var result = _usersRepository.Update(input);

            Assert.NotNull(result);
            Assert.Equal("123456789", result.Password);
        }

        [Fact]
        public void ShouldReturnNullUpdate()
        {
            var input = new User()
            {
                Id = Guid.NewGuid(),
                CreationDate = DateTime.Now,
                Password = "1234",
                Salt = "1234",
                FirstName = "NonExistent",
                LastName = "User",
                RoleId = 1,
                UserName = "nonexistentuser123"
            };

            var result = _usersRepository.Update(input);

            Assert.Null(result);
        }

        [Fact]
        public void ShouldDelete()
        {
            var input = _usersRepository.GetByUserName("deleteme123");

            if (input == null)
            {
                input = _usersRepository.Add(new User()
                {
                    Id = new Guid("d7fd88c8-dc4a-4b42-bc1c-c26a9a1a8f09"),
                    CreationDate = DateTime.Now,
                    Password = "1234",
                    Salt = "1234",
                    FirstName = "Deletion",
                    LastName = "User",
                    RoleId = 1,
                    UserName = "deleteme123"
                });
            }

            var result = _usersRepository.Delete(input.Id);

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldReturnNullDelete()
        {
            var result = _usersRepository.Delete(Guid.Empty);

            Assert.Null(result);
        }
    }
}