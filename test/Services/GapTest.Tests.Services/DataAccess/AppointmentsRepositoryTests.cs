using System;
using System.Collections.Generic;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using GapTest.Services.DataAccess.Implementations;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GapTest.Tests.Services.DataAccess
{
    public class AppointmentRepositoryTests
    {
        private readonly IAppointmentsRepository _appointmentsRepository;

        public AppointmentRepositoryTests()
        {
            var configurationValues = new Dictionary<string, string>
            {
                {
                    "AppSettings:ConnectionString",
                    "Server=localhost;Database=ClinicDates;User Id=sa;Password=Michoacan123*;"
                }
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationValues)
                .Build();

            _appointmentsRepository = new AppointmentsRepository(configuration);
        }

        [Fact]
        public void ShouldGetAllAppointments()
        {
            var result = _appointmentsRepository.GetAll();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldGetById()
        {
            var result = _appointmentsRepository.GetById(new Guid("65894697-0296-478f-a21d-9e0f82687364"));

            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldGetNullById()
        {
            var result = _appointmentsRepository.GetById(Guid.Empty);

            Assert.Null(result);
        }

        [Fact]
        public void ShouldGetByUserId()
        {
            var result = _appointmentsRepository.GetByUserId(new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"));
            
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
        
        [Fact]
        public void ShouldInsert()
        {
            var id = new Guid("687b0538-fce6-401a-9e4f-19f626917dae");
            
            if (_appointmentsRepository.GetById(id) != null)
            {
                _appointmentsRepository.Delete(id);
            }
            
            var input = new Appointment()
            {
                Id = id,
                AppointmentDate = DateTime.Now.AddDays(30),
                CreationDate = DateTime.Now,
                UserId = new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"),
                AppointmentTypeId = 1
            };
            
            var result = _appointmentsRepository.Add(input);

            Assert.NotNull(result);
        }
        
        [Fact]
        public void ShouldUpdate()
        {
            var id = new Guid("65894697-0296-478f-a21d-9e0f82687364");
            
            var input = _appointmentsRepository.GetById(id);

            if (input == null)
            {
                input = _appointmentsRepository.Add(new Appointment()
                {
                    Id = id,
                    AppointmentDate = DateTime.Now.AddDays(30),
                    CreationDate = DateTime.Now,
                    UserId = new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"),
                    AppointmentTypeId = 1
                });
            }

            input.AppointmentDate = DateTime.Now.AddDays(100);

            var result = _appointmentsRepository.Update(input);

            Assert.NotNull(result);
        }
        
        [Fact]
        public void ShouldReturnNullUpdate()
        {
            var input = new Appointment()
            {
                Id = Guid.NewGuid(),
                AppointmentDate = DateTime.Now.AddDays(30),
                CreationDate = DateTime.Now,
                UserId = new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"),
                AppointmentTypeId = 1
            };
            
            var result = _appointmentsRepository.Update(input);

            Assert.Null(result);
        }
        
        [Fact]
        public void ShouldDeleteRole()
        {
            var id = new Guid("331bfddd-8135-4673-af00-0cad962f4ca2");
            
            var input = _appointmentsRepository.GetById(id);

            if (input == null)
            {
                input = _appointmentsRepository.Add(new Appointment()
                {
                    Id = id,
                    AppointmentDate = DateTime.Now.AddDays(30),
                    CreationDate = DateTime.Now,
                    UserId = new Guid("030a70c1-8050-463a-a744-b1a058d1d5d5"),
                    AppointmentTypeId = 1
                });
            }
            
            var result = _appointmentsRepository.Delete(input.Id);

            Assert.NotNull(result);
        }
        
        [Fact]
        public void ShouldReturnNullDelete()
        {
            var result = _appointmentsRepository.Delete(Guid.Empty);

            Assert.Null(result);
        }
    }
}