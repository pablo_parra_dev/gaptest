using System;
using System.Collections.Generic;
using System.Linq;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using GapTest.Services.DataAccess.Implementations;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GapTest.Tests.Services.DataAccess
{
    public class AppointmentTypesRepositoryTests
    {
        private readonly IAppointmentTypesRepository _appointmentTypesRepository;

        public AppointmentTypesRepositoryTests()
        {
            var configurationValues = new Dictionary<string, string>
            {
                {
                    "AppSettings:ConnectionString",
                    "Server=localhost;Database=ClinicDates;User Id=sa;Password=Michoacan123*;"
                }
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationValues)
                .Build();

            _appointmentTypesRepository = new AppointmentTypesRepository(configuration);
        }

        [Fact]
        public void ShouldGetAllAppointmentTypes()
        {
            var result = _appointmentTypesRepository.GetAll();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldGetAppointmentTypeById()
        {
            var result = _appointmentTypesRepository.GetById(1);

            Assert.NotNull(result);
            Assert.Equal("Medicina general", result.Description);
        }

        [Fact]
        public void ShouldGetNullById()
        {
            var result = _appointmentTypesRepository.GetById(10000);

            Assert.Null(result);
        }

        [Fact]
        public void ShouldGetActiveDefault()
        {
            var result = _appointmentTypesRepository.GetActive();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldGetActiveExplicit()
        {
            var result = _appointmentTypesRepository.GetActive(true);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldInsertRole()
        {
            var input = new AppointmentType()
            {
                Description = "Test appointment type",
                Active = true
            };

            var result = _appointmentTypesRepository.Add(input);

            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
        }

        [Fact]
        public void ShouldUpdate()
        {
            var input = _appointmentTypesRepository.GetActive()
                .FirstOrDefault(a => a.Description.Contains("Test appointment type"));

            if (input == null)
            {
                input = _appointmentTypesRepository.Add(new AppointmentType()
                {
                    Description = "Test appointment type",
                    Active = true
                });
            }

            input.Description = "Test appointment type updated";
            input.Active = !input.Active;

            var result = _appointmentTypesRepository.Update(input);

            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
        }

        [Fact]
        public void ShouldReturnNullUpdate()
        {
            var input = new AppointmentType()
            {
                Id = Int32.MaxValue,
                Active = false,
                Description = "Non existent role"
            };

            var result = _appointmentTypesRepository.Update(input);

            Assert.Null(result);
        }

        [Fact]
        public void ShouldDeleteRole()
        {
            var input = _appointmentTypesRepository.GetActive()
                .FirstOrDefault(a => a.Description.Contains("Test appointment type"));

            if (input == null)
            {
                input = _appointmentTypesRepository.Add(new AppointmentType()
                {
                    Description = "Test appointment type",
                    Active = true
                });
            }

            var result = _appointmentTypesRepository.Delete(input.Id);

            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
        }

        [Fact]
        public void ShouldReturnNullDelete()
        {
            var result = _appointmentTypesRepository.Delete(Int32.MaxValue);

            Assert.Null(result);
        }
    }
}