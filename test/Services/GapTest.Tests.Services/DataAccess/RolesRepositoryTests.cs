using System;
using System.Collections.Generic;
using System.Linq;
using GapTest.Services.DataAccess;
using GapTest.Services.DataAccess.Entities;
using GapTest.Services.DataAccess.Implementations;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GapTest.Tests.Services.DataAccess
{
    public class RolesRepositoryTests
    {
        private readonly IRolesRepository _rolesRepository;
        
        public RolesRepositoryTests()
        {
            var configurationValues = new Dictionary<string, string>
            {
                {
                    "AppSettings:ConnectionString",
                    "Server=localhost;Database=ClinicDates;User Id=sa;Password=Michoacan123*;"
                }
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationValues)
                .Build();

            _rolesRepository = new RolesRepository(configuration);
        }

        [Fact]
        public void ShouldGetAllRoles()
        {
            var result = _rolesRepository.GetAll();

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void ShouldGetRoleById()
        {
            var result = _rolesRepository.GetById(1);

            Assert.NotNull(result);
            Assert.Equal("Administrador", result.Description);
        }

        [Fact]
        public void ShouldGetNullById()
        {
            var result = _rolesRepository.GetById(10000);

            Assert.Null(result);
        }
        
        [Fact]
        public void ShouldGetActiveRolesDefault()
        {
            var result = _rolesRepository.GetActive();
            
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
        
        [Fact]
        public void ShouldGetActiveRolesExplicit()
        {
            var result = _rolesRepository.GetActive(true);
            
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
        
        [Fact]
        public void ShouldInsertRole()
        {
            var role = new Role()
            {
                Description = "Test role",
                Active = true
            };
            
            var result = _rolesRepository.Add(role);

            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
        }
        
        [Fact]
        public void ShouldUpdateRole()
        {
            var role = _rolesRepository.GetActive().FirstOrDefault(a => a.Description.Contains("Test role"));

            if (role == null)
            {
                role = _rolesRepository.Add(new Role()
                {
                    Description = "Test role",
                    Active = true
                });
            }

            role.Description = "Test role updated";
            role.Active = !role.Active;

            var result = _rolesRepository.Update(role);

            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
        }
        
        [Fact]
        public void ShouldReturnNullUpdate()
        {
            var role = new Role()
            {
                Id = Int32.MaxValue,
                Active = false,
                Description = "Non existent role"
            };
            
            var result = _rolesRepository.Update(role);

            Assert.Null(result);
        }
        
        [Fact]
        public void ShouldDeleteRole()
        {
            var role = _rolesRepository.GetActive().FirstOrDefault(a => a.Description.Contains("Test role"));

            if (role == null)
            {
                role = _rolesRepository.Add(new Role()
                {
                    Description = "Test role",
                    Active = true
                });
            }
            
            var result = _rolesRepository.Delete(role.Id);

            Assert.NotNull(result);
            Assert.NotEqual(0, result.Id);
        }
        
        [Fact]
        public void ShouldReturnNullDelete()
        {
            var result = _rolesRepository.Delete(Int32.MaxValue);

            Assert.Null(result);
        }
    }
}