CREATE TABLE dbo.Roles
(
	Id INT IDENTITY(1,1) NOT NULL,
	Description NVARCHAR(50) NOT NULL,
	Active BIT NOT NULL CONSTRAINT DF_Roles_Active DEFAULT 1,
	CONSTRAINT PK_Roles PRIMARY KEY(Id)
)
GO

SET IDENTITY_INSERT dbo.Roles ON
GO

INSERT INTO dbo.Roles (Id, Description)
VALUES (1, 'Administrador'), (2, 'Paciente')
GO

SET IDENTITY_INSERT dbo.Roles OFF
GO

CREATE TABLE dbo.AppointmentTypes
(
	Id INT IDENTITY(1,1) NOT NULL,
	Description NVARCHAR(50) NOT NULL,
	Active BIT NOT NULL CONSTRAINT DF_AppointmentType_Active DEFAULT 1,
	CONSTRAINT PK_AppointmentTypes PRIMARY KEY (Id)
)
GO

SET IDENTITY_INSERT dbo.AppointmentTypes ON
GO

INSERT INTO dbo.AppointmentTypes (Id, Description)
VALUES (1, 'Medicina general'), (2, 'Odontología'), (3, 'Pediatría'), (4, 'Neurología')
GO

SET IDENTITY_INSERT dbo.AppointmentTypes OFF
GO

CREATE TABLE dbo.Users
(
	Id UNIQUEIDENTIFIER NOT NULL,
	RoleId INT NOT NULL,
	FirstName NVARCHAR(30) NOT NULL,
	LastName NVARCHAR(30) NOT NULL,
	UserName NVARCHAR(30) NOT NULL,
	Password NVARCHAR(100) NOT NULL,
	Salt NVARCHAR(100) NOT NULL,
	CreationDate DATETIME NOT NULL CONSTRAINT DF_Users_CreationDate DEFAULT GETDATE(),
	CONSTRAINT PK_Users PRIMARY KEY (Id),
	CONSTRAINT FK_Users_Roles FOREIGN KEY (RoleId) REFERENCES dbo.Roles (Id)
)
GO

CREATE TABLE dbo.Appointments
(
	Id UNIQUEIDENTIFIER NOT NULL,
	UserId UNIQUEIDENTIFIER NOT NULL,
	AppointmentTypeId INT NOT NULL,
	AppointmentDate DATETIME NOT NULL,
	CreationDate DATETIME NOT NULL CONSTRAINT DF_Appointments_CreationDate DEFAULT GETDATE(),
	CONSTRAINT PK_Appointments PRIMARY KEY (Id),
	CONSTRAINT FK_Appointments_Users FOREIGN KEY (UserId) REFERENCES dbo.Users (Id),
	CONSTRAINT FK_Appointments_AppointmentTypes FOREIGN KEY (AppointmentTypeId) REFERENCES dbo.AppointmentTypes (Id)
)
GO