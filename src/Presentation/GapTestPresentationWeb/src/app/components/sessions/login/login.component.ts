import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UserCredentials } from '../../../models'
import { MyErrorStateMatcher } from 'src/app/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'], 
  providers: [MyErrorStateMatcher]
})
export class LoginComponent implements OnInit { 
  spinnerVisible = false;

  userFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(20)
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(16)
  ]);

  matcher = new MyErrorStateMatcher();

  constructor() {     
  }

  ngOnInit(): void {
  }

  onSubmit(){
    this.spinnerVisible = true;
    setTimeout(() => {
      this.spinnerVisible = false;
      console.warn(this.userFormControl.value);
      console.warn(this.passwordFormControl.value);
    }, 3000);   
  }

}
