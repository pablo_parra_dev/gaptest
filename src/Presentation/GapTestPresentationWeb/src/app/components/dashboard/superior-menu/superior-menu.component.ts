import { Component, OnInit, Input } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-superior-menu',
  templateUrl: './superior-menu.component.html',
  styleUrls: ['./superior-menu.component.css']
})
export class SuperiorMenuComponent implements OnInit {
  @Input('showMenu') showMenu: boolean;
  @Input('lateralMenu') lateralMenu: MatSidenav;

  constructor() { }

  ngOnInit(): void {
  }

  onClickMenu(){    
    this.lateralMenu.toggle();
  }
}
