import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MyErrorStateMatcher, AppDateAdapter, APP_DATE_FORMATS } from 'src/app/services';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { ValidateDate, ValidateAppointmentType } from 'src/app/validators/validators';
import { AppointmentType } from 'src/app/models';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-appointment-creation',
  templateUrl: './appointment-creation.component.html',
  styleUrls: ['./appointment-creation.component.css'],
  providers: [MyErrorStateMatcher,
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class AppointmentCreationComponent implements OnInit {

  appointmentTypes: Array<AppointmentType> = new Array<AppointmentType>();
  filteredAppointmentTypes: Observable<Array<AppointmentType>>;

  dateFormControl: FormControl = new FormControl('', [
    Validators.required, ValidateDate
  ]);

  timeFormControl: FormControl = new FormControl('', [
    Validators.required
  ]);

  typeControl: FormControl = new FormControl('', [
    Validators.required,
    ValidateAppointmentType
  ])

  formGroup = new FormGroup({
    date: this.dateFormControl,
    time: this.timeFormControl,
    type: this.typeControl
  });

  matcher = new MyErrorStateMatcher();

  constructor() {
    this.appointmentTypes.push(new AppointmentType(1, 'Medicina general'));
    this.appointmentTypes.push(new AppointmentType(2, 'Odontología'));
    this.appointmentTypes.push(new AppointmentType(3, 'Pediatría'));
    this.appointmentTypes.push(new AppointmentType(4, 'Neurología'));
  }

  ngOnInit(): void {
    this.filteredAppointmentTypes = this.typeControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value))
    )
  }

  typeDisplayFunction(type: AppointmentType) {
    return type && type.description ? type.description : '';
  }

  onCreateAppointment() {
    if (this.formGroup.valid) {
      console.log('Appointment created');
    } else {
      console.log('Appointment not created');
    }
  }

  onCancelCreate() {
    console.log('Canceled creation');
  }

  private filter(value: string): Array<AppointmentType> {    
    return this.appointmentTypes.filter(option => option.description.toLowerCase().indexOf(value) >= 0);
  }
}
