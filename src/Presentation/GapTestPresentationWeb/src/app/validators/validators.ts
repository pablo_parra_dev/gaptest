import { AbstractControl } from '@angular/forms';
import { AppointmentType } from '../models';

export function ValidateDate(control: AbstractControl) {
    const date = new Date(control.value);
    const actualDate = new Date();
    if (actualDate > date) {
        return { validDate: true }
    }
    return null;
}

export function ValidateAppointmentType(control: AbstractControl) {
    const appointmentType: AppointmentType = control.value;
    if (appointmentType.id == null) {
        return { validAppointmentType: true }
    }
    return null;
}

export function ValidateUrl(control: AbstractControl) {
    if (!control.value.startsWith('https') || !control.value.includes('.io')) {
        return { validUrl: true };
    }
    return null;
}