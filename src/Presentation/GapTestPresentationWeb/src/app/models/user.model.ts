export class User{
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    roleId: string;
    token: string;
    creationDate: Date;
}