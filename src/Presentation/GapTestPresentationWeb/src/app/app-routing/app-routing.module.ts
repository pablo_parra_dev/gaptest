import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router/';
import { HomeComponent } from '../components/dashboard';
import { PatientDetailComponent } from '../components/patients';
import { AppointmentsListComponent, AppointmentCreationComponent } from '../components/appointments';
import { LoginComponent } from '../components/sessions';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTokenInterceptor } from '../interceptors';
import { JwtService, AuthGuard, UserService } from '../services';
import { NotFoundComponent } from '../components/not-found';
import { NoAuthGuard } from '../services/no-auth-guard.service';

const routes: Routes = [
  {
    path: 'dashboard', component: HomeComponent, children: [
      { path: 'patient', component: PatientDetailComponent },
      { path: 'appointments', component: AppointmentsListComponent, canActivate: [AuthGuard] },
      { path: 'appointments/creation', component: AppointmentCreationComponent, canActivate: [AuthGuard] },
      { path: '', pathMatch: 'full', redirectTo: '/dashboard/patient' },
      { path: '**', pathMatch: 'full', redirectTo: '/dashboard/patient' },
    ]
  },
  {
    path: 'login', component: LoginComponent, canActivate: [NoAuthGuard]
  },
  { path: '', pathMatch: 'full', redirectTo: '/dashboard/patient' },
  { path: '**', pathMatch: 'full', component: NotFoundComponent}
]

@NgModule({
  declarations: [],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    AuthGuard,
    UserService,
    JwtService
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
