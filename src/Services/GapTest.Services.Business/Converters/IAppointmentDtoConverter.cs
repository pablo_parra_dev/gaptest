using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters
{
    public interface IAppointmentDtoConverter : IGenericConverter<Appointment, AppointmentDto>
    {
    }
}