using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters.Implementations
{
    public class UserDtoConverter : GenericConverter<User, UserDto>, IUserDtoConverter
    {
        protected override UserDto ConvertToDto(User entity)
        {
            return new UserDto()
            {
                Id = entity.Id,
                CreationDate = entity.CreationDate,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                RoleId = entity.RoleId,
                UserName = entity.UserName
            };
        }

        protected override User ConvertToEntity(UserDto dto)
        {
            return new User()
            {
                Id = dto.Id,
                CreationDate = dto.CreationDate,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                RoleId = dto.RoleId,
                UserName = dto.UserName
            };
        }
    }
}