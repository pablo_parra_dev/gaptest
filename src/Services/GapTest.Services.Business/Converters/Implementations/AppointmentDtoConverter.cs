using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters.Implementations
{
    public class AppointmentDtoConverter : GenericConverter<Appointment, AppointmentDto>, IAppointmentDtoConverter
    {
        protected override Appointment ConvertToEntity(AppointmentDto dto)
        {
            return new Appointment()
            {
                Id = dto.Id,
                AppointmentDate = dto.AppointmentDate,
                CreationDate = dto.CreationDate,
                UserId = dto.UserId,
                AppointmentTypeId = dto.AppointmentTypeId
            };
        }

        protected override AppointmentDto ConvertToDto(Appointment entity)
        {
            return new AppointmentDto()
            {
                Id = entity.Id,
                AppointmentDate = entity.AppointmentDate,
                CreationDate = entity.CreationDate,
                UserId = entity.UserId,
                AppointmentTypeId = entity.AppointmentTypeId
            };
        }
    }
}