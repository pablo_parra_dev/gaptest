using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters.Implementations
{
    public class RoleDtoConverter : GenericConverter<Role, RoleDto>, IRoleDtoConverter
    {
        protected override RoleDto ConvertToDto(Role entity)
        {
            return new RoleDto()
            {
                Active = entity.Active,
                Description = entity.Description,
                Id = entity.Id
            };
        }

        protected override Role ConvertToEntity(RoleDto dto)
        {
            return new Role()
            {
                Active = dto.Active,
                Description = dto.Description,
                Id = dto.Id
            };
        }
    }
}