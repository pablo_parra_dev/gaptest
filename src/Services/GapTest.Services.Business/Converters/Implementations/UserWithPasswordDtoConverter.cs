using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters.Implementations
{
    public class UserWithPasswordDtoConverter : GenericConverter<User, UserWithPasswordDto>, IUserWithPasswordDtoConverter
    {
        private readonly IUserDtoConverter _userDtoConverter;

        public UserWithPasswordDtoConverter(IUserDtoConverter userDtoConverter)
        {
            _userDtoConverter = userDtoConverter;
        }

        protected override UserWithPasswordDto ConvertToDto(User entity)
        {
            UserWithPasswordDto dto = (UserWithPasswordDto) _userDtoConverter.ToDto(entity);
            dto.Password = entity.Password;
            dto.Salt = entity.Salt;

            return dto;
        }

        protected override User ConvertToEntity(UserWithPasswordDto dto)
        {
            User entity = _userDtoConverter.ToEntity(dto);
            entity.Password = dto.Password;
            entity.Salt = dto.Salt;

            return entity;
        }
    }
}