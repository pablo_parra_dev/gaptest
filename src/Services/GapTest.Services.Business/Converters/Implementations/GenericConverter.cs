using System.Collections.Generic;
using System.Linq;
using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters.Implementations
{
    public abstract class GenericConverter<TEntity, TDto> : IGenericConverter<TEntity, TDto>
        where TEntity : IEntity where TDto : IDto
    {
        public TDto ToDto(TEntity entity)
        {
            return ConvertToDto(entity);
        }

        public TEntity ToEntity(TDto dto)
        {
            return ConvertToEntity(dto);
        }

        public IEnumerable<TDto> ToDto(IEnumerable<TEntity> entities)
        {
            return entities?.Select(a => ConvertToDto(a));
        }

        public IEnumerable<TEntity> ToEntity(IEnumerable<TDto> dtos)
        {
            return dtos?.Select(a => ConvertToEntity(a));
        }

        protected abstract TEntity ConvertToEntity(TDto dto);

        protected abstract TDto ConvertToDto(TEntity entity);
    }
}