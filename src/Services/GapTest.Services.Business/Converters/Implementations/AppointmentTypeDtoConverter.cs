using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters.Implementations
{
    public class AppointmentTypeDtoConverter : GenericConverter<AppointmentType, AppointmentTypeDto>, IAppointmentTypeDtoConverter
    {
        protected override AppointmentTypeDto ConvertToDto(AppointmentType entity)
        {
            return new AppointmentTypeDto()
            {
                Active = entity.Active,
                Description = entity.Description,
                Id = entity.Id
            };
        }

        protected override AppointmentType ConvertToEntity(AppointmentTypeDto dto)
        {
            return new AppointmentType()
            {
                Active = dto.Active,
                Description = dto.Description,
                Id = dto.Id
            };
        }
    }
}