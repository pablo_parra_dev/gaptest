using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters
{
    public interface IAppointmentTypeDtoConverter : IGenericConverter<AppointmentType, AppointmentTypeDto>
    {
    }
}