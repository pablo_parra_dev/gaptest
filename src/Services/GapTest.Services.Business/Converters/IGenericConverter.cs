using System.Collections.Generic;
using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.Business.Converters
{
    public interface IGenericConverter<TEntity, TDto> where TEntity : IEntity where TDto : IDto
    {
        TDto ToDto(TEntity entity);

        TEntity ToEntity(TDto dto);

        IEnumerable<TDto> ToDto(IEnumerable<TEntity> entities);

        IEnumerable<TEntity> ToEntity(IEnumerable<TDto> dtos);
    }
}