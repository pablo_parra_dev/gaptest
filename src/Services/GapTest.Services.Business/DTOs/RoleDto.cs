namespace GapTest.Services.Business.DTOs
{
    public class RoleDto : IDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }
    }
}