using System;

namespace GapTest.Services.Business.DTOs
{
    public class AppointmentDto : IDto
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public int AppointmentTypeId { get; set; }

        public DateTime AppointmentDate { get; set; }

        public DateTime CreationDate { get; set; }
    }
}