namespace GapTest.Services.Business.DTOs
{
    public class UserWithPasswordDto : UserDto
    {
        public string Password { get; set; }

        public string Salt { get; set; }
    }
}