using System;

namespace GapTest.Services.Business.DTOs
{
    public class UserDto : IDto
    {
        public Guid Id { get; set; }

        public int RoleId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public DateTime CreationDate { get; set; }
    }
}