using System.Collections.Generic;
using GapTest.Services.Business.DTOs;

namespace GapTest.Services.Business
{
    public interface IRoleService
    {
        IEnumerable<RoleDto> GetActiveRoles();

        RoleDto GetRoleById(int id);
    }
}