using System;
using System.Collections.Generic;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Commons;

namespace GapTest.Services.Business
{
    public interface IAppointmentService
    {
        IEnumerable<AppointmentDto> GetUserAppointments(Guid userId);

        ValidatableResponse<AppointmentDto> CreateNewAppointment(AppointmentDto dto);

        ValidatableResponse<AppointmentDto> CancelAppointment(Guid appointmentId);
    }
}