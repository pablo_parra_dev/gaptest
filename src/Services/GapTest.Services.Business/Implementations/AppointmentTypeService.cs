using System.Collections.Generic;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess;

namespace GapTest.Services.Business.Implementations
{
    public class AppointmentTypeService : IAppointmentTypeService
    {
        private readonly IAppointmentTypesRepository _appointmentTypesRepository;
        private readonly IAppointmentTypeDtoConverter _appointmentTypeDtoConverter;

        public AppointmentTypeService(IAppointmentTypesRepository appointmentTypesRepository,
            IAppointmentTypeDtoConverter appointmentTypeDtoConverter)
        {
            _appointmentTypesRepository = appointmentTypesRepository;
            _appointmentTypeDtoConverter = appointmentTypeDtoConverter;
        }

        public IEnumerable<AppointmentTypeDto> GetActiveAppointmentTypes()
        {
            var result = _appointmentTypesRepository.GetActive();

            return _appointmentTypeDtoConverter.ToDto(result);
        }

        public AppointmentTypeDto GetAppointmentTypeById(int id)
        {
            var result = _appointmentTypesRepository.GetById(id);

            return _appointmentTypeDtoConverter.ToDto(result);
        }
    }
}