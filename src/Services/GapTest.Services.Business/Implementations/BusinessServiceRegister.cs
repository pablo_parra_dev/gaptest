using GapTest.Services.Business.Converters;
using GapTest.Services.Business.Converters.Implementations;
using GapTest.Services.Commons;
using Microsoft.Extensions.DependencyInjection;

namespace GapTest.Services.Business.Implementations
{
    public class BusinessServiceRegister : IServiceRegister
    {
        public void RegisterService(IServiceCollection services)
        {
            services.AddTransient<IAppointmentService, AppointmentService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAppointmentTypeService, AppointmentTypeService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IAppointmentDtoConverter, AppointmentDtoConverter>();
            services.AddTransient<IAppointmentTypeDtoConverter, AppointmentTypeDtoConverter>();
            services.AddTransient<IRoleDtoConverter, RoleDtoConverter>();
            services.AddTransient<IUserDtoConverter, UserDtoConverter>();
            services.AddTransient<IUserWithPasswordDtoConverter, UserWithPasswordDtoConverter>();
            
        }
    }
}