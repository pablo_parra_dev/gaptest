using System;
using System.Collections.Generic;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Crypto;
using GapTest.Services.DataAccess;

namespace GapTest.Services.Business.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IUserDtoConverter _userDtoConverter;
        private readonly IUserWithPasswordDtoConverter _userWithPasswordDtoConverter;
        private readonly IRfc2898HashService _hashService;

        public UserService(IUsersRepository usersRepository, IUserDtoConverter userDtoConverter,
            IUserWithPasswordDtoConverter userWithPasswordDtoConverter, IRfc2898HashService hashService)
        {
            _usersRepository = usersRepository;
            _userDtoConverter = userDtoConverter;
            _userWithPasswordDtoConverter = userWithPasswordDtoConverter;
            _hashService = hashService;
        }

        public bool ValidateCredentials(string userName, string password)
        {
            var user = _usersRepository.GetByUserName(userName);

            if (user == null)
            {
                return false;
            }

            return user.Password == _hashService.GenerateHash(password, user.Salt);
        }

        public UserWithPasswordDto GetUserWithPasswordById(Guid id)
        {
            var result = _usersRepository.GetById(id);

            return _userWithPasswordDtoConverter.ToDto(result);
        }

        public UserDto GetUserById(Guid id)
        {
            var result = _usersRepository.GetById(id);

            return _userDtoConverter.ToDto(result);
        }

        public UserDto GetUserByUserName(string userName)
        {
            var result = _usersRepository.GetByUserName(userName);

            return _userDtoConverter.ToDto(result);
        }

        public IEnumerable<UserDto> GetAllUsers()
        {
            var result = _usersRepository.GetAll();

            return _userDtoConverter.ToDto(result);
        }
    }
}