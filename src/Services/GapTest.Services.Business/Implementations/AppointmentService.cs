using System;
using System.Collections.Generic;
using System.Linq;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Commons;
using GapTest.Services.DataAccess;

namespace GapTest.Services.Business.Implementations
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IAppointmentsRepository _appointmentsRepository;
        private readonly IAppointmentDtoConverter _appointmentDtoConverter;

        public AppointmentService(IAppointmentsRepository appointmentsRepository,
            IAppointmentDtoConverter appointmentDtoConverter)
        {
            _appointmentsRepository = appointmentsRepository;
            _appointmentDtoConverter = appointmentDtoConverter;
        }

        public IEnumerable<AppointmentDto> GetUserAppointments(Guid userId)
        {
            var result = _appointmentsRepository.GetByUserId(userId);

            return _appointmentDtoConverter.ToDto(result);
        }

        public ValidatableResponse<AppointmentDto> CreateNewAppointment(AppointmentDto dto)
        {
            if (dto.AppointmentDate < DateTime.Now)
            {
                return ValidatableResponseExtensions<AppointmentDto>.SingleError("PAST_APPOINTMENT_DATE");
            }
            
            var userAppointments = GetUserAppointments(dto.UserId);

            var existAppointmentSameDate = userAppointments.Any(a =>
                new DateTime(a.AppointmentDate.Year, a.AppointmentDate.Month, a.AppointmentDate.Day)
                == new DateTime(dto.AppointmentDate.Year, dto.AppointmentDate.Month, dto.AppointmentDate.Day));

            if (existAppointmentSameDate)
            {
                return ValidatableResponseExtensions<AppointmentDto>.SingleError("DUPLICATED_APPOINTMENT");
            }

            var appointmentEntity = _appointmentDtoConverter.ToEntity(dto);

            var result = _appointmentsRepository.Add(appointmentEntity);

            var resultDto = _appointmentDtoConverter.ToDto(result);

            return ValidatableResponseExtensions<AppointmentDto>.Ok(resultDto);
        }

        public ValidatableResponse<AppointmentDto> CancelAppointment(Guid appointmentId)
        {
            var appointment = _appointmentsRepository.GetById(appointmentId);

            if (appointment == null)
            {
                return ValidatableResponseExtensions<AppointmentDto>.SingleError("APPOINTMENT_NOT_FOUND");
            }

            if ((appointment.AppointmentDate - DateTime.Now).TotalHours < 24)
            {
                return ValidatableResponseExtensions<AppointmentDto>.SingleError("APPOINTMENT_CANCELLATION_TIMEOUT");
            }

            var result = _appointmentsRepository.Delete(appointmentId);

            var resultDto = _appointmentDtoConverter.ToDto(result);

            return ValidatableResponseExtensions<AppointmentDto>.Ok(resultDto);
        }
    }
}