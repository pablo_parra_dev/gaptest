using System.Collections.Generic;
using GapTest.Services.Business.Converters;
using GapTest.Services.Business.DTOs;
using GapTest.Services.DataAccess;

namespace GapTest.Services.Business.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly IRolesRepository _rolesRepository;
        private readonly IRoleDtoConverter _roleDtoConverter;

        public RoleService(IRolesRepository rolesRepository, IRoleDtoConverter roleDtoConverter)
        {
            _rolesRepository = rolesRepository;
            _roleDtoConverter = roleDtoConverter;
        }

        public IEnumerable<RoleDto> GetActiveRoles()
        {
            var result = _rolesRepository.GetActive();

            return _roleDtoConverter.ToDto(result);
        }

        public RoleDto GetRoleById(int id)
        {
            var result = _rolesRepository.GetById(id);

            return _roleDtoConverter.ToDto(result);
        }
    }
}