using System.Collections.Generic;
using GapTest.Services.Business.DTOs;

namespace GapTest.Services.Business
{
    public interface IAppointmentTypeService
    {
        IEnumerable<AppointmentTypeDto> GetActiveAppointmentTypes();

        AppointmentTypeDto GetAppointmentTypeById(int id);
    }
}