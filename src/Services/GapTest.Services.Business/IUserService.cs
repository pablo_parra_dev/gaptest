using System;
using System.Collections.Generic;
using GapTest.Services.Business.DTOs;

namespace GapTest.Services.Business
{
    public interface IUserService
    {
        bool ValidateCredentials(string userName, string password);

        UserWithPasswordDto GetUserWithPasswordById(Guid id);

        UserDto GetUserById(Guid id);

        UserDto GetUserByUserName(string userName);

        IEnumerable<UserDto> GetAllUsers();
    }
}