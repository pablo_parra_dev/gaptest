using System;
using System.Collections.Generic;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.DataAccess
{
    public interface IAppointmentsRepository : IGenericRepository<Appointment>
    {
        IEnumerable<Appointment> GetByUserId(Guid userId);
    }
}