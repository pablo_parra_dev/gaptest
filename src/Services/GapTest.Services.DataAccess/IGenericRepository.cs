using System.Collections.Generic;

namespace GapTest.Services.DataAccess
{
    public interface IGenericRepository<T> where T : class, new()
    {
        T Add(T input);

        T Update(T input);

        T GetById(object id);
        
        T Delete(object id);

        IEnumerable<T> GetAll();
    }
}