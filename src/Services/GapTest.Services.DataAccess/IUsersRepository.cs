using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.DataAccess
{
    public interface IUsersRepository : IGenericRepository<User>
    {
        User GetByUserName(string username);
    }
}