using System;

namespace GapTest.Services.DataAccess.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class TableNameAttribute : Attribute
    {
        public TableNameAttribute(string name)
        {
            TableName = name;
        }

        public string TableName { get; }
    }
}