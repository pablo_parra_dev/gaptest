namespace GapTest.Services.DataAccess.Attributes
{
    public enum KeyType
    {
        Normal = 0,
        Identity = 1
    }
}