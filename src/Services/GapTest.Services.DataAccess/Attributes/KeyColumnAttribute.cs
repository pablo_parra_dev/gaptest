using System;

namespace GapTest.Services.DataAccess.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class KeyColumnAttribute : Attribute
    {
        public KeyColumnAttribute(KeyType type = KeyType.Normal)
        {
            KeyType = type;
        }

        public KeyType KeyType { get; }
    }
}