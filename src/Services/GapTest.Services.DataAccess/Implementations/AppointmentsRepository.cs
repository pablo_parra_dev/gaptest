using System;
using System.Collections.Generic;
using Dapper;
using GapTest.Services.DataAccess.Entities;
using Microsoft.Extensions.Configuration;

namespace GapTest.Services.DataAccess.Implementations
{
    public class AppointmentsRepository : GenericRepository<Appointment>, IAppointmentsRepository
    {
        public AppointmentsRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Appointment> GetByUserId(Guid userId)
        {
            using (var connection = CreateConnection())
            {
                var result = connection.Query<Appointment>($"SELECT * FROM {TableName} WHERE UserId = @UserId AND AppointmentDate > GETDATE()",
                    new {UserId = userId});

                return result;
            }
        }
    }
}