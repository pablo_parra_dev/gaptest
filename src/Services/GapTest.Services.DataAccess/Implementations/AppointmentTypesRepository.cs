using System.Collections.Generic;
using Dapper;
using GapTest.Services.DataAccess.Entities;
using Microsoft.Extensions.Configuration;

namespace GapTest.Services.DataAccess.Implementations
{
    public class AppointmentTypesRepository : GenericRepository<AppointmentType>, IAppointmentTypesRepository
    {
        public AppointmentTypesRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<AppointmentType> GetActive(bool active = true)
        {
            using (var connection = CreateConnection())
            {
                var result =
                    connection.Query<AppointmentType>($"SELECT * FROM {TableName} WHERE Active = @Active",
                        new {Active = active});

                return result;
            }
        }
    }
}