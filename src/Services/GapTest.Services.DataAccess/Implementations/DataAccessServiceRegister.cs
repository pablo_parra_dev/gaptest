using GapTest.Services.Commons;
using Microsoft.Extensions.DependencyInjection;

namespace GapTest.Services.DataAccess.Implementations
{
    public class DataAccessServiceRegister : IServiceRegister
    {
        public void RegisterService(IServiceCollection services)
        {
            services.AddTransient<IAppointmentsRepository, AppointmentsRepository>();
            services.AddTransient<IUsersRepository, UsersRepository>();
            services.AddTransient<IAppointmentTypesRepository, AppointmentTypesRepository>();
            services.AddTransient<IRolesRepository, RolesRepository>();
        }
    }
}