using Dapper;
using GapTest.Services.DataAccess.Entities;
using Microsoft.Extensions.Configuration;

namespace GapTest.Services.DataAccess.Implementations
{
    public class UsersRepository : GenericRepository<User>, IUsersRepository
    {
        public UsersRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public User GetByUserName(string username)
        {
            using (var connection = base.CreateConnection())
            {
                var result = connection.QuerySingleOrDefault<User>(
                    $"SELECT * FROM {TableName} WHERE UserName = @UserName", new {UserName = username});

                return result;
            }
        }
    }
}