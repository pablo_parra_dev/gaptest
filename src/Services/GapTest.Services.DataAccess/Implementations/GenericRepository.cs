using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using Dapper;
using GapTest.Services.DataAccess.Attributes;
using Microsoft.Extensions.Configuration;

namespace GapTest.Services.DataAccess.Implementations
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        private const string DefaultKeyName = "Id";
        private readonly string _tableName;
        private readonly string _keyColumnName;
        private readonly string _connectionString;

        protected GenericRepository(IConfiguration configuration)
        {
            _tableName = GetTableName();
            _keyColumnName = GetKeyName();
            string connectionString = configuration.GetSection("AppSettings").GetSection("ConnectionString").Value;

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Should be parametrized", nameof(connectionString));
            }

            _connectionString = connectionString;
        }

        protected string TableName => _tableName;

        public T Add(T input)
        {
            var insertQuery = GenerateInsertQuery();

            using (var connection = CreateConnection())
            {
                var result = connection.QuerySingleOrDefault<T>(insertQuery, input);

                return result;
            }
        }

        public T Update(T input)
        {
            var updateQuery = GenerateUpdateQuery();

            using (var connection = CreateConnection())
            {
                var result = connection.QuerySingleOrDefault<T>(updateQuery, input);

                return result;
            }
        }

        public T GetById(object id)
        {
            using (var connection = CreateConnection())
            {
                var parameter = new Dictionary<string, object>()
                {
                    {_keyColumnName, id}
                };

                var result =
                    connection.QuerySingleOrDefault<T>(
                        $"SELECT * FROM {_tableName} WHERE {_keyColumnName} = @{_keyColumnName}", parameter);

                return result;
            }
        }

        public T Delete(object id)
        {
            using (var connection = CreateConnection())
            {
                var parameter = new Dictionary<string, object>()
                {
                    {_keyColumnName, id}
                };

                var result =
                    connection.QuerySingleOrDefault<T>(
                        $"DELETE FROM {_tableName} OUTPUT Deleted.* WHERE {_keyColumnName} = @{_keyColumnName}",
                        parameter);

                return result;
            }
        }

        public IEnumerable<T> GetAll()
        {
            using (var connection = CreateConnection())
            {
                var result = connection.Query<T>($"SELECT * FROM {_tableName}");

                return result;
            }
        }

        public SqlConnection SqlConnection()
        {
            return new SqlConnection(_connectionString);
        }

        protected IDbConnection CreateConnection()
        {
            var connection = SqlConnection();
            connection.Open();

            return connection;
        }

        private IEnumerable<PropertyInfo> GetProperties()
        {
            return typeof(T).GetProperties();
        }

        private string GenerateInsertQuery()
        {
            List<PropertyInfo> properties = GetProperties().ToList();

            StringBuilder builder = new StringBuilder($"INSERT INTO {_tableName} (");

            foreach (var property in properties)
            {
                if (IsIdentityKey(property))
                {
                    continue;
                }

                builder.Append($"[{property.Name}]");
                builder.Append(",");
            }

            builder.Remove(builder.Length - 1, 1)
                .Append(") OUTPUT Inserted.* VALUES (");

            foreach (var property in properties)
            {
                if (IsIdentityKey(property))
                {
                    continue;
                }

                builder.Append($"@{property.Name}");
                builder.Append(",");
            }

            builder.Remove(builder.Length - 1, 1)
                .Append(")");

            return builder.ToString();
        }

        private string GenerateUpdateQuery()
        {
            List<PropertyInfo> properties = GetProperties().ToList();

            StringBuilder builder = new StringBuilder($"UPDATE {_tableName} SET ");

            foreach (var property in properties)
            {
                if (property.Name == _keyColumnName)
                {
                    continue;
                }

                builder.Append($"[{property.Name}] = @{property.Name},");
            }

            builder.Remove(builder.Length - 1, 1)
                .Append($" OUTPUT Inserted.* WHERE {_keyColumnName}=@{_keyColumnName}");

            return builder.ToString();
        }

        private string GetTableName()
        {
            var attribute =
                typeof(T).GetCustomAttributes(typeof(TableNameAttribute), false).FirstOrDefault() as TableNameAttribute;

            if (attribute != null)
            {
                return attribute.TableName;
            }

            return typeof(T).Name;
        }

        private string GetKeyName()
        {
            List<PropertyInfo> properties = GetProperties().ToList();

            foreach (var property in properties)
            {
                var keyColumnAttribute =
                    (KeyColumnAttribute) property.GetCustomAttribute(typeof(KeyColumnAttribute), false);

                if (keyColumnAttribute != null)
                {
                    return property.Name;
                }
            }

            return DefaultKeyName;
        }

        private bool IsIdentityKey(PropertyInfo property)
        {
            var keyColumnAttribute =
                (KeyColumnAttribute) property.GetCustomAttribute(typeof(KeyColumnAttribute), false);

            return keyColumnAttribute?.KeyType == KeyType.Identity;
        }
    }
}