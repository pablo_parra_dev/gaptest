using System.Collections.Generic;
using Dapper;
using GapTest.Services.DataAccess.Entities;
using Microsoft.Extensions.Configuration;

namespace GapTest.Services.DataAccess.Implementations
{
    public class RolesRepository : GenericRepository<Role>, IRolesRepository
    {
        public RolesRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Role> GetActive(bool active = true)
        {
            using (var connection = CreateConnection())
            {
                var result = connection.Query<Role>($"SELECT * FROM {TableName} WHERE Active = @Active",
                    new {Active = active});

                return result;
            }
        }
    }
}