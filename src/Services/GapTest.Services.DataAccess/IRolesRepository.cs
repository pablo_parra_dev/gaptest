using System.Collections.Generic;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.DataAccess
{
    public interface IRolesRepository : IGenericRepository<Role>
    {
        IEnumerable<Role> GetActive(bool active = true);
    }
}