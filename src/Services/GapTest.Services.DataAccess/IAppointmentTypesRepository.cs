using System.Collections.Generic;
using GapTest.Services.DataAccess.Entities;

namespace GapTest.Services.DataAccess
{
    public interface IAppointmentTypesRepository : IGenericRepository<AppointmentType>
    {
        IEnumerable<AppointmentType> GetActive(bool active = true);
    }
}