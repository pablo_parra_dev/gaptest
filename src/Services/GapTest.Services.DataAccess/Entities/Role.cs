using GapTest.Services.DataAccess.Attributes;

namespace GapTest.Services.DataAccess.Entities
{
    [TableName("Roles")]
    public class Role : IEntity
    {
        [KeyColumn(KeyType.Identity)]
        public int Id { get; set; }
        
        public string Description { get; set; }
        
        public bool Active { get; set; }
    }
}