using System;
using GapTest.Services.DataAccess.Attributes;

namespace GapTest.Services.DataAccess.Entities
{
    [TableName("Users")]
    public class User : IEntity
    {
        [KeyColumn]
        public Guid Id { get; set; }

        public int RoleId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }
        
        public DateTime CreationDate { get; set; }
    }
}