using System;
using GapTest.Services.DataAccess.Attributes;

namespace GapTest.Services.DataAccess.Entities
{
    [TableName("Appointments")]
    public class Appointment : IEntity
    {
        [KeyColumn]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public int AppointmentTypeId { get; set; }

        public DateTime AppointmentDate { get; set; }

        public DateTime CreationDate { get; set; }
    }
}