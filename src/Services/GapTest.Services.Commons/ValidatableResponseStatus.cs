namespace GapTest.Services.Commons
{
    public enum ValidatableResponseStatus
    {
        Ok = 0,
        Failed = 1
    }
}