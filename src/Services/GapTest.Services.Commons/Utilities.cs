using System.Security.Cryptography;

namespace GapTest.Services.Commons
{
    public static class Utilities
    {
        private static RandomNumberGenerator _randomNumber = RandomNumberGenerator.Create();

        public static byte[] Random(int countBytes)
        {
            var ret = new byte[countBytes];

            lock (_randomNumber)
                _randomNumber.GetBytes(ret);

            return ret;
        }
    }
}