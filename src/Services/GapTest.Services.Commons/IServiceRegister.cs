using Microsoft.Extensions.DependencyInjection;

namespace GapTest.Services.Commons
{
    public interface IServiceRegister
    {
        void RegisterService(IServiceCollection services);
    }
}