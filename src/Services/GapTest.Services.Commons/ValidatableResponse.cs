using System.Collections.Generic;

namespace GapTest.Services.Commons
{
    public class ValidatableResponse<T> where T : class
    {
        public T Response { get; set; }

        public IEnumerable<string> Errors { get; set; }
        
        public ValidatableResponseStatus Status { get; set; } 
    }
}