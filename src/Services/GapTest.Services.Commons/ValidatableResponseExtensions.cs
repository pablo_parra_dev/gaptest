using System.Collections.Generic;
using System.Linq;

namespace GapTest.Services.Commons
{
    public static class ValidatableResponseExtensions<T> where T : class
    {
        public static ValidatableResponse<T> Ok(T response)
        {
            return new ValidatableResponse<T>()
            {
                Status = ValidatableResponseStatus.Ok,
                Errors = null,
                Response = response
            };
        }
        
        public static ValidatableResponse<T> SingleError(string error)
        {
            return new ValidatableResponse<T>()
            {
                Status = ValidatableResponseStatus.Failed,
                Errors = new List<string>() { error },
                Response = null
            };
        }
        
        public static ValidatableResponse<T> Errors(params string[] errors)
        {
            return new ValidatableResponse<T>()
            {
                Status = ValidatableResponseStatus.Failed,
                Errors = errors.ToList(),
                Response = null
            };
        }
    }
}