using System;

namespace GapTest.Services.Tokenization
{
    public interface ITokenizationService
    {
        string GenerateToken(Guid userId, string roleName);
    }
}