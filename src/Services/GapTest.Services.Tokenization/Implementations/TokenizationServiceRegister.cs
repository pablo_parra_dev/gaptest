using GapTest.Services.Commons;
using GapTest.Services.Tokenization.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace GapTest.Services.Tokenization.Implementations
{
    public class TokenizationServiceRegister : IServiceRegister
    {
        public void RegisterService(IServiceCollection services)
        {
            services.AddTransient<ITokenizationService, JwtTokenizationService>();
        }
    }
}