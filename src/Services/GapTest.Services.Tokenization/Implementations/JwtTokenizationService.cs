using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace GapTest.Services.Tokenization.Implementations
{
    public class JwtTokenizationService : ITokenizationService
    {
        private readonly string _secret;
        
        public JwtTokenizationService(IConfiguration configuration)
        {
            string secret = configuration.GetSection("AppSettings").GetSection("JWT_SECRET").Value;

            if (string.IsNullOrEmpty(secret))
            {
                throw new ArgumentException("Should be parametrized", secret);
            }

            _secret = secret;
        }

        public string GenerateToken(Guid userId, string roleName)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userId.ToString()),
                    new Claim(ClaimTypes.Role, roleName)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}