using GapTest.Services.Commons;
using Microsoft.Extensions.DependencyInjection;

namespace GapTest.Services.Crypto.Implementations
{
    public class CryptoServiceRegister : IServiceRegister
    {
        public void RegisterService(IServiceCollection services)
        {
            services.AddTransient<IRfc2898HashService, Rfc2898HashService>();
        }
    }
}