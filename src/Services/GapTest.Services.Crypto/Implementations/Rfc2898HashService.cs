using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using GapTest.Services.Commons;

namespace GapTest.Services.Crypto.Implementations
{
    public class Rfc2898HashService : IRfc2898HashService
    {
        private readonly int SaltSize = 16;
        private readonly int DefaultIterations = 1000;

        public string GenerateHash(string text, string salt)
        {
            return Hash(text, salt);
        }

        public string GenerateHash(string text, out string salt)
        {
            salt = GenerateSalt();

            return Hash(text, salt);
        }

        public string GenerateSalt(int? iterations = null)
        {
            iterations ??= DefaultIterations;

            byte[] saltBytes = Utilities.Random(SaltSize);

            var iters = iterations.Value.ToString("X");

            return $"{iters}.{Convert.ToBase64String(saltBytes)}";
        }

        private string Hash(string text, string salt)
        {
            string[] values = salt.Split('.');
            var iterations = int.Parse(values[0], NumberStyles.HexNumber);
            var saltValue = values[1];

            using (var pbkdf2 = new Rfc2898DeriveBytes(
                Encoding.UTF8.GetBytes(text),
                Convert.FromBase64String(saltValue),
                iterations))
            {
                var key = pbkdf2.GetBytes(24);

                return Convert.ToBase64String(key);
            }
        }
    }
}