namespace GapTest.Services.Crypto
{
    public interface IHashService
    {
        string GenerateHash(string text, string salt);

        string GenerateHash(string text, out string salt);

        string GenerateSalt(int? iterations = null);
    }
}