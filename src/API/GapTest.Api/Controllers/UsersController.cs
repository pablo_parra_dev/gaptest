using System;
using System.Linq;
using System.Security.Claims;
using GapTest.Api.Models;
using GapTest.Services.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GapTest.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet()]
        [Authorize(Roles = Roles.Admin)]
        public IActionResult GetAll()
        {
            var result = _userService.GetAllUsers();

            return Ok(result);
        }
    }
}