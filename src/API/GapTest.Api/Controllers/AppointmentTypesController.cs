using GapTest.Services.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GapTest.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AppointmentTypesController : Controller
    {
        private readonly IAppointmentTypeService _appointmentTypeService;

        public AppointmentTypesController(IAppointmentTypeService appointmentTypeService)
        {
            _appointmentTypeService = appointmentTypeService;
        }
        
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_appointmentTypeService.GetActiveAppointmentTypes());
        }
    }
}