using GapTest.Services.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GapTest.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class RolesController : Controller
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_roleService.GetActiveRoles());
        }
    }
}