using GapTest.Api.Models;
using GapTest.Services.Business;
using GapTest.Services.Tokenization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GapTest.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LoginController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly ITokenizationService _tokenizationService;

        public LoginController(IUserService userService, IRoleService roleService, ITokenizationService tokenizationService)
        {
            _userService = userService;
            _roleService = roleService;
            _tokenizationService = tokenizationService;
        }
        
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post(UserPasswordAuth authCredentials)
        {
            var validateCredentials = _userService.ValidateCredentials(authCredentials.UserName, authCredentials.Password);

            if (!validateCredentials)
            {
                return Unauthorized();
            }

            var user = _userService.GetUserByUserName(authCredentials.UserName);
            var role = _roleService.GetRoleById(user.RoleId);

            string token = _tokenizationService.GenerateToken(user.Id, role.Description);
            
            return Ok(token);
        }
    }
}