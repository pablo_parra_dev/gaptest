using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using GapTest.Services.Business;
using GapTest.Services.Business.DTOs;
using GapTest.Services.Commons;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GapTest.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AppointmentController : Controller
    {
        private const string AppointmentNotFoundError = "APPOINTMENT_NOT_FOUND";
        private const string AppointmentTimeoutError = "APPOINTMENT_CANCELLATION_TIMEOUT";
        private const string PastAppointmentDateError = "PAST_APPOINTMENT_DATE";
        private const string DuplicateAppointmentError = "DUPLICATED_APPOINTMENT";
        private readonly IAppointmentService _appointmentService;

        public AppointmentController(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                return Unauthorized();
            }

            var appointments = _appointmentService.GetUserAppointments(new Guid(userId));

            return Ok(appointments);
        }

        [HttpDelete("{appointmentId}")]
        public IActionResult Delete(Guid appointmentId)
        {
            var result = _appointmentService.CancelAppointment(appointmentId);

            if (result.Status != ValidatableResponseStatus.Failed)
            {
                return Ok(result.Response);
            }

            switch (result.Errors.FirstOrDefault())
            {
                case AppointmentNotFoundError:
                    return NotFound();
                case AppointmentTimeoutError:
                    return StatusCode((int) HttpStatusCode.Locked);
                default:
                    return StatusCode((int) HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] AppointmentDto dto)
        {
            var result = _appointmentService.CreateNewAppointment(dto);

            if (result.Status == ValidatableResponseStatus.Ok)
            {
                return Ok(result.Response);
            }
            
            switch (result.Errors.FirstOrDefault())
            {
                case PastAppointmentDateError:
                    return BadRequest();
                case DuplicateAppointmentError:
                    return Conflict();
                default:
                    return StatusCode((int) HttpStatusCode.InternalServerError);
            }
        }
    }
}