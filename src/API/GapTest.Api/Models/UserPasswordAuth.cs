namespace GapTest.Api.Models
{
    public class UserPasswordAuth
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}